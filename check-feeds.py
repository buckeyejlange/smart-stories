import feedparser
import importlib
import logging
from data.feeds import get_sample_feeds, to_sources
from settings import load_settings

logging.basicConfig(level=logging.INFO)

# TODO: abstract this out somewhere else.
settings = load_settings('./settings.json')
data_module = importlib.import_module(settings['sourceData']['module'])
source_repo_class = getattr(data_module, 'SourceRepo')
feed_repo_class = getattr(data_module, 'FeedRepo')

feed_repo = feed_repo_class(settings['sourceData']['connectionString'])
source_repo = source_repo_class(settings['sourceData']['connectionString'])

# TODO: Encapsulate into debug flag at some point
saved_feeds = feed_repo.get_all()
feeds = None

if len(saved_feeds) < 1:
    logging.info('No feeds available, getting from debug')
    feeds = get_sample_feeds()
    for f in feeds:
        feed_repo.save(f)
else:
    feeds = saved_feeds

for feed in feeds:
    data = feedparser.parse(feed['url'])
    sources = to_sources(data, feed)
    source_repo.save_if_newer(sources)
