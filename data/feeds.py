from datetime import timezone
from dateutil import parser
from uuid import uuid4
import json


def get_sample_feeds():
    return [
        {'id': str(uuid4()), 'feed_title': 'Ars Technica', 'url': 'http://feeds.arstechnica.com/arstechnica/index'},
        {'id': str(uuid4()), 'feed_title': 'NPR', 'url': 'https://www.npr.org/rss/rss.php?id=1001'},
        {'id': str(uuid4()), 'feed_title': 'Engadget', 'url': 'https://www.engadget.com/rss.xml'}
    ]


def to_sources(feed_data, feed_entry):
    return [{
        'id': str(uuid4()),
        'feed_id': feed_entry['id'],
        'date': parser.parse(e['published']).replace(tzinfo=timezone.utc),
        'data': json.dumps(e)
    } for e in feed_data.entries]
