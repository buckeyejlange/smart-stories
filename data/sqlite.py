from datetime import datetime, timezone
from dateutil import parser
import logging
import sqlite3


class FeedRepo:
    __create_table_query = """
        CREATE TABLE Feeds (
            Id TEXT PRIMARY KEY,
            FeedTitle TEXT,
            Url TEXT
        )
    """

    __get_all_query = """
        SELECT *
        FROM Feeds
    """

    __save_query = """
        INSERT INTO Feeds
        VALUES (?, ?, ?)
    """

    __table_exists_query = "SELECT name FROM sqlite_master WHERE type='table' AND name='Feeds'"

    def __init__(self, connection_string):
        self.connection_string = connection_string
        self.__init_db()

    def get_all(self):
        with self.__create_conn() as conn:
            conn.row_factory = sqlite3.Row
            results = conn.execute(FeedRepo.__get_all_query)
            return results.fetchall()

    def save(self, feed):
        with self.__create_conn() as conn:
            conn.execute(FeedRepo.__save_query, [feed['id'], feed['feed_title'], feed['url']])
            conn.commit()

    def __create_conn(self):
        return sqlite3.connect(self.connection_string)

    def __init_db(self):
        with self.__create_conn() as conn:
            result = conn.execute(FeedRepo.__table_exists_query)

            if len(result.fetchall()) < 1:
                logging.info(f'No database found for {self.connection_string} . Creating...')
                conn.execute(FeedRepo.__create_table_query)

            conn.commit()


class SourceRepo:
    __create_table_query = """
                    CREATE TABLE Sources (
                        Id TEXT PRIMARY KEY,
                        FeedId TEXT,
                        Date DATE,
                        Data TEXT,
                        FOREIGN KEY (FeedId) REFERENCES Feeds(Id)
                    )
                """

    __get_last_save_query = """
        SELECT Date
        FROM Sources
        WHERE FeedId = ?
        ORDER BY Date DESC
        LIMIT 1
    """

    __save_query = """
        INSERT INTO Sources
        VALUES (?,?,?,?)
    """

    __table_exists_query = "SELECT name FROM sqlite_master WHERE type='table' AND name='Sources'"

    def __init__(self, connection_string):
        self.connection_string = connection_string
        self.__init_db()

    def save_if_newer(self, sources):
        if len(sources) < 1:
            # no reason to do anything
            return

        with sqlite3.connect(self.connection_string) as conn:
            feed_id = sources[0]['feed_id']
            last_queried = SourceRepo.__get_last_date(conn, feed_id)
            params = [[source['id'], source['feed_id'], source['date'], source['data']]
                      for source in sources if source['date'] > last_queried]

            if len(params) > 0:
                logging.info(f'Adding {len(params)} entries for {feed_id}')
                conn.executemany(SourceRepo.__save_query, params)
                conn.commit()
            else:
                logging.info(f'No entries added for {feed_id}')

    @staticmethod
    def __get_last_date(conn, feed_id):
        result = conn.execute(SourceRepo.__get_last_save_query, [feed_id])
        row = result.fetchone()

        if row is not None:
            return parser.parse(row[0]).replace(tzinfo=timezone.utc)

        return datetime.min.replace(tzinfo=timezone.utc)

    def __init_db(self):
        with sqlite3.connect(self.connection_string) as conn:
            result = conn.execute(SourceRepo.__table_exists_query)

            if len(result.fetchall()) < 1:
                logging.info(f'No database found for {self.connection_string} . Creating...')
                conn.execute(SourceRepo.__create_table_query)

            conn.commit()
